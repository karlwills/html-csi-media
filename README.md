# HTML/CSS Layout task

---

To get up and running, please run `npm install` in the root directory.

Once this has completed, feel free to run `parcel index.html` to spin up a local server to view the site if needed.